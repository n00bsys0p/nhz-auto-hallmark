#!/bin/bash
NHZ_VER="3.8"
DIR="/opt/horizon"
SERVER_IP=$(wget -qO- http://ifconfig.me/ip)
DATE=$(date +%Y-%m-%d)
OWD=$(pwd)
DOWNLOAD_SERVER=${DOWNLOAD_SERVER:-"http://downloads.horizonplatform.io/binaries"}

# Prepare
echo "Preparing OS"
apt-get clean && apt-get update && apt-get upgrade -y
apt-get install -y python jq openjdk-7-jre unzip screen curl iptables-persistent
mkdir -p "$DIR"
cd "$DIR"

# Download and unpack
echo "Downloading and unpacking software"
wget $DOWNLOAD_SERVER/hz_v${NHZ_VER}_api.zip
unzip hz_v${NHZ_VER}_api.zip 

# Configure
echo "Configuring node"
cd hz-api
cp $OWD/nhz.properties $(pwd)/conf/
sed -i "s/nhz.myAddress=/nhz.myAddress=$SERVER_IP/g" $(pwd)/conf/nhz.properties
chmod +x $(pwd)/run.sh

# Start the server, unhallmarked
screen -d -m -S HZ_HALLMARK $(pwd)/run.sh
echo "WAITING FOR SERVER (30 seconds)"

# Wait for the node to start
for i in $(seq 1 15); do
    echo -ne '.'
    sleep 2
done
echo -ne '\n'
echo "DONE"
sleep 2

# Generate/Inject Hallmark
read -p "Do you wish to insert the hallmark, or secret phrase?
1. Hallmark Code
2. Secret Phrase
? " HMSP

case $HMSP in
    "1")
    read -p "Pleast insert the hallmark code: " CODE
    sed -i "s/nhz.myHallmark=/nhz.myHallmark=$CODE/g" $(pwd)/conf/nhz.properties
    ;;
    "2")
    echo "Your Secret Phrase will be not be stored locally."
    read -p "Please insert the Secret Phrase for the account you wish to hallmark: " PASSPHRASE
    CODE=$(curl -s -d requestType="markHost" -d host="$SERVER_IP" -d weight="100" -d date="$DATE" -d secretPhrase="$PASSPHRASE" http://127.0.0.1:7776/nhz | jq -r '.hallmark')
    sed -i "s/nhz.myHallmark=/nhz.myHallmark=$CODE/g" $(pwd)/conf/nhz.properties
esac

sleep 2

# Kill the node temporarily
screen -S HZ_HALLMARK -X quit
pkill -15 screen

# Set up Firewall
echo "Setting up Firewall"
cat $OWD/ipt.fw > /etc/iptables/rules.v4
service iptables-persistent restart

# Add group and user
echo "Adding service user"
useradd -M -r -s /usr/sbin/nologin -c "NHZ User" horizon
chown -R horizon:horizon $(pwd)

read -p "Enter a username for the login user: " LOGIN_USER
# Add login user
useradd -s /bin/bash -m -c "Login User" $LOGIN_USER
echo "Setting the password for the login user. Type carefully and keep this password safe, or you may lose access to the server."
passwd $LOGIN_USER

echo "Configuring SSHD to block root logins"
sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
service ssh restart

echo "Installing Upstart job"
# Install Upstart job
cp $OWD/horizon.conf /etc/init/

# Start the Hallmarked NHZ Node
service horizon start

sleep 1
cat <<EOL
DONATE :

===============================================================
//      CREDITED : ANDIK IBRAHIMI / XANDER SHEPHERD          //
// NHZ : NHZ-WFZE-R2L9-M4LN-FSWVK / NHZ-NSY9-7KYT-WUNU-AUNCE //
================================================================
EOL

exit 0
